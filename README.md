[![pipeline status](https://gitlab.com/training_hrb/gitlab_gke_starter/badges/master/pipeline.svg)](https://gitlab.com/training_hrb/gitlab_gke_starter/commits/master)
[![coverage report](https://gitlab.com/training_hrb/gitlab_gke_starter/badges/master/coverage.svg)](https://gitlab.com/training_hrb/gitlab_gke_starter/commits/master)

# gitlab_gke_starter

An easy to follow project linked to GKE (Google Kubernetes Engine)
