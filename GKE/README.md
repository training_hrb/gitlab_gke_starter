# GKE Cluster

## Setup
```console
$ gcloud config set project [GKE_PROJECT_ID]
```

Cluster of 3 nodes having the following specs (it will take some time, depends on your bandwidth), meanwhile if you wish to know all zones/regions (have a look [HERE](https://cloud.google.com/compute/docs/regions-zones/) )

```console
$ gcloud compute machine-types list | awk '{ if ( $1 == "NAME" || ($1 == "n1-standard-2" && $2 == "europe-west1-b")){print} }'

NAME             ZONE                       CPUS  MEMORY_GB  DEPRECATED
n1-standard-2    europe-west1-b             2     7.50
```

Once sure of the node types you'll be using, hit https://gitlab.com/[GROUPE]/[PROJECT]/clusters, Cluster section and do like the follwing :

![GKE Gitlab cluster creation](./screenshots/GKE_Gitlab_workshop_created.png) 

`Note` : For example's sake this setup was put, production env should be different

After waiting for a couple of minutes, you should have the cluster created on both :
* __GCP (Google Cloud Platform)__

![GCP Cluster named gitlab](./screenshots/GKE_Gitlab_workshop_created.png)

* __Gitlab__

![Gitlab Cluster named gitlab](./screenshots/GKE_Gitlab_creation_finished.png)

## Configuration

Once the cluster is up and running, you'll have to configure two important sections :
* __Environment Scope__ : straight forward from the [docs](https://gitlab.com/help/user/project/clusters/index#setting-the-environment-scope-premium) : `When adding more than one Kubernetes cluster to your project, you need to differentiate
them with an environment scope. The environment scope associates clusters with environments similar to how the
environment-specific variables work.
The default environment scope is *, which means all jobs, regardless of their
environment, will use that cluster. Each scope can only be used by a single
cluster in a project, and a validation error will occur if otherwise.
Also, jobs that don't have an environment keyword set will not be able to access any cluster.`
 
* __Base domain__ : Simply put, associate your external endpoint with a wildcard DNS (e.g *.extia.gke.com) in
order to reach your apps. In my case, I've used [xip.io](xip.io).

* the third and final one is the __applications__ : for starter, it's mandatory to install Helm tiller, 
`a tool for managing kubernetes charts (packages pre-configured Kubernetes resources)`, which Gitlab uses 
to install the rest of management applications, like :
    * [Prometheus](https://prometheus.io/docs/introduction/overview/)
    * [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/)
    * [Cert manager](https://cert-manager.readthedocs.io/en/latest/#)
    * and so on...

`note` : If you prefer to hack your way into ingress, and prefer instead of using a public domain name, an address
or perhaps a local DNS record (/etc/hosts), there is a way to get the IP address (I assume your letting Gitlab manage your cluster if not change the namespace): 
```console
$ kubectl get service --namespace=gitlab-managed-apps ingress-nginx-ingress-controller -o jsonpath='{.status.loadBalancer.ingress[0].ip}'
``` 


                
