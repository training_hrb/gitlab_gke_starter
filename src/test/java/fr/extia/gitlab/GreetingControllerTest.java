package fr.extia.gitlab;


import fr.extia.gitlab.Controller.GreetingController;
import org.junit.Assert;
import org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GreetingControllerTest {

    @Autowired
    GreetingController greetingController;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setup() {
        Assert.assertNotNull(this.greetingController);
        Assert.assertNotNull(this.restTemplate);
    }

    @Test
    public void greetingShouldReturnUserName() throws Exception {
        String userName = "Hamza",
                apiVersion = "/api/v1";
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/" + apiVersion + "/" + userName,
                String.class).equals(String.format("Hello %s", userName)));
    }


}
