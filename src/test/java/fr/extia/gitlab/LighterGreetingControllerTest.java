package fr.extia.gitlab;


import fr.extia.gitlab.Controller.GreetingController;
import org.junit.Assert;
import org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class LighterGreetingControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void livenessProbeShouldBeOk() throws Exception {
        this.mockMvc.perform(get("/")).andExpect(status().isOk());
    }


    @Test
    public void shouldReturnDefaultMessage() throws Exception {
        String userName = "Hamza";

        this.mockMvc.perform(get(String.format("/api/v1/Hamza"))).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString(String.format("Hello %s", userName))));
    }

}
