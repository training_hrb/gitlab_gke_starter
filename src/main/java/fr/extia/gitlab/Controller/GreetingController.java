package fr.extia.gitlab.Controller;

import fr.extia.gitlab.PathConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "/")
public class GreetingController {

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity livenessProbe() {
        return ResponseEntity.ok().build();
    }

    @GetMapping(PathConstants.apiVersion + "/{name}")
    @ResponseStatus(HttpStatus.OK)
    public String sayHi(final @PathVariable String name) {
        return String.format("Hello %s", name);
    }
}
