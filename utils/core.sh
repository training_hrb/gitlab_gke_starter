#!/usr/bin/env bash

# Git push options are only available with Git 2.10 or newer
# Have a look at this : https://docs.gitlab.com/ce/user/project/merge_requests/#git-push-options
create_merge_request(){
  if [ "$#" -ne 2 ];then
    echo -e "Usage: you should at least put 2 arguments !\n\ttitle : MR title\n\tbranch_name: Target MR branch" >&2
    exit 1
  fi
  title=$1
  branch_name=$2
  git push -o merge_request.title="$title" \
   -o merge_request.create \
   -o merge_request.target="$branch_name" \
   -o merge_request.merge_when_pipeline_succeeds \
   -o merge_request.remove_source_branch origin  "$branch_name"
}

_ingress_ip_address(){
  kubectl get service --namespace=gitlab-managed-apps ingress-nginx-ingress-controller -o jsonpath='{.status.loadBalancer.ingress[0].ip}'
}

#create_merge_request 'Utility Shell script' 'master'