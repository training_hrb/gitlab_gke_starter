#!/usr/bin/env bash

# For this basic script to run, you should install the follwing : 
#   1. Gcloud
#   2. kubectl
#   3. Link Gcloud to your GKE cluster

# This way, you let kubectl handle everything for you in a single file
_exportGKECluster() {
    kubectl get all --export -o yaml >gitlab_cluster.yaml
}

# This function will export each ressource speratly in a folder/YAML file
_atomicExportGKECluster() {
    for n in $(kubectl get -o=name pvc,configmap,serviceaccount,secret,ingress,service,deployment,statefulset,hpa,job,cronjob); do
        mkdir -p $(dirname $n)
        kubectl get -o=yaml --export $n >$n.yaml
    done
}